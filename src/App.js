//import { Fragment } from 'react';
import { BrowserRouter  as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import { Container } from 'react-bootstrap'
import Login from './pages/Login';
import Signup from './pages/Signup';
import Landing from './pages/Landing';
import './App.css';

function App() {
  return (

    <Router>
        
        <Container>
          <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/landing" component={Landing} />
           
          </Switch>
        </Container>
        
      </Router>
  );
}

export default App;
