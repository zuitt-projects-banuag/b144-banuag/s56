import { Container, Button, Row, Col } from 'react-bootstrap';
import ImgLamp from '../image/lamp.png';
import ImgTv from '../image/tv.png';
import ImgSpeaker from '../image/speaker.png';
import ImgCooker from '../image/cooker.png';
import ImgFan from '../image/fan.png';
import ImgRef from '../image/ref.png';
import ImgAircon from '../image/aircon.png';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import React, { Fragment } from 'react';
import { Link, NavLink } from 'react-router-dom';

export default function Landing(){
  return(
  <Fragment>
   <Navbar bg="light" variant="light">
    <Container>
    <Navbar.Brand href="#home">Navbar</Navbar.Brand>
    <Nav className="me-auto">
      <Nav.Link href="#home">Home</Nav.Link>
      <Nav.Link href="#features">New Arrivals</Nav.Link>
      <Nav.Link href="#pricing">Logout</Nav.Link>
    </Nav>
    </Container>
  </Navbar>

    <Container>

      <div className = "img-lamp">
        <img src= {ImgLamp} 
         alt="logo"
         height={300}
         width={350} />
    </div>
      <h1 className="text-center header">Aesthetic Home Interior</h1>
      <h4 className="text-center small-header">Starting at ₱ 500.00</h4>

      <Button className="landing-button" variant="secondary" type="submit">
        SHOP NOW
    </Button>

    <h4 className="text-center text-header">New Arrivals</h4>
    <Row className = "images" xs={12} md={3} mb-5>
        
        <img src= {ImgTv} 
         alt="logo"
         height={300}
         width={400} />

         <img src= {ImgSpeaker} 
         alt="logo"
         height={300}
         width={300} />

         <img src= {ImgCooker} 
         alt="logo"
         height={300}
         width={400} />
         

         <img src= {ImgFan} 
         alt="logo"
         height={300}
         width={400} />

         <img src= {ImgRef} 
         alt="logo"
         height={300}
         width={400} />

         <img src= {ImgAircon} 
         alt="logo"
         height={300}
         width={400} />

    </Row>
    </Container>
     </Fragment>
  )
}