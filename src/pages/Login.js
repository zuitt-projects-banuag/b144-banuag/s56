import { Form, Button, Container} from 'react-bootstrap';
import Img from '../image/logo.png';
export default function Login(){


  return (
<Form className = "form">
    <Container className="Form">
    <div className = "img-container">
        <img src= {Img} 
        alt="logo"
         height={200}
        width={250} />
    </div>
    
    <h1 className="brand-name mt-5">Welcome to OnlineShop</h1>
    
    
    <Form.Group className="mb-3 mt-5" controlId="formBasicEmail">
        <Form.Control className="text-box" type="email" placeholder="Enter email" />
    </Form.Group>
    
    <Form.Group className="mb-3" controlId="formBasicPassword"> 
        <Form.Control type="password" placeholder="Enter Password" />
    </Form.Group>
   
    
    <Form.Group className="mb-3" controlId="formForgotPassword">
        <Form.Label><a href="">Forgot Password?</a></Form.Label>  
    </Form.Group>
    

    <div className="button">
    <Button variant="secondary" type="submit">
        LOGIN
    </Button>
    </div>

    <Form.Group>
    <Form.Label className="mt-3">Don't have an account?</Form.Label>
    </Form.Group>

    <Form.Group controlId="formAccount">
        <Form.Label className="accountLabel"><a href="">Create account</a></Form.Label>
    </Form.Group>
    
    </Container>
</Form>
  )
}
